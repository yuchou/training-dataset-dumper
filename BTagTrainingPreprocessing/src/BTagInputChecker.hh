#ifndef BTAG_INPUT_CHECKER_HH
#define BTAG_INPUT_CHECKER_HH

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackParticleContainer.h"


namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

class BTagInputChecker
{
public:
  BTagInputChecker();
  unsigned nMissingTracks(const xAOD::Jet& jet);
  bool hasTracks(const xAOD::Jet& jet);
  bool hasBTagging(const xAOD::Jet& jet);
private:
  typedef SG::AuxElement AE;
  typedef std::vector<ElementLink<xAOD::TrackParticleContainer>> TrackLinks;
  AE::ConstAccessor<TrackLinks> m_trackAssociator;
};
#endif
